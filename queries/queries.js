const { ApolloServer, gql } = require("apollo-server-express");
const { GraphQLDateTime } = require("graphql-iso-date");
const bcrypt = require("bcryptjs");

// mongoose models
const User = require("../models/User");
const Venue = require("../models/Venue");
const CateringService = require("../models/CateringService");
const Transaction = require("../models/Transaction");


const customScalarResolver = {
	Date: GraphQLDateTime
};

const typeDefs = gql`

	scalar Date

	type UserType {
		id: ID!
		firstName: String!
		lastName: String!
		contactNumber: String
		email: String!
		password: String!
		role: String
	}

	type VenueType {
		id: String!
		name: String!
		floorArea: Int!
		maxGuest: Int!
		description: String!
		price: Int!
		
	}

	type CateringServiceType {
		id: String!
		packageName: String!
		pricePerHead: Int!
		description: String!
		menu: String!
		
	}

	type TransactionType {
		id: ID!
		venueId: String!
		venue: VenueType
		userId: String
		eventType: String!
		guestNumber: Int!
		cateringService: String!
		date: Date!
		time: String!
		
	}

	type Query {
		getUsers: [UserType]
		getVenues: [VenueType]
		getCateringServices: [CateringServiceType]
		getTransactions: [TransactionType]

		getUser(id: ID!): UserType
		getVenue(id: String): VenueType
		getCateringService(id: ID!): CateringServiceType
		getTransaction(id: ID!): TransactionType
	}

	type Mutation {

		createUser(
			firstName: String!
			lastName: String!
			contactNumber: String
			email: String!
			password: String!
			role: String
		): UserType

		createVenue(
			name: String!
			floorArea: Int!
			maxGuest: Int!
			description: String!
			price: Int!
		): VenueType

		
		createCateringService(
			packageName: String!
			pricePerHead: Int!
			description: String!
			menu: String!
		): CateringServiceType

		createTransaction(
			venueId: String!
			userId: String
			eventType: String!
			guestNumber: Int!
			cateringService: String!
			date: Date!
			time: String!
		): TransactionType


		updateUser(
			id: ID!
			firstName: String!
			lastName: String!
			contactNumber: String
			email: String!
			password: String!
			role: String
		): UserType

		updateVenue(
			id: String!
			name: String!
			floorArea: Int!
			maxGuest: Int!
			description: String!
			price: Int!
		): VenueType

		
		updateCateringService(
			id: String!
			packageName: String!
			pricePerHead: Int!
			description: String!
			menu: String!
			
		): CateringServiceType

		updateTransaction(
			id: ID!
			venueId: String!
			userId: String!
			eventType: String!
			guestNumber: Int!
			cateringService: String!
			date: Date!
			time: String!
		): TransactionType

		deleteUser(id:String):Boolean
		deleteVenue(id:String):Boolean
		deleteCateringService(id:String):Boolean
		deleteTransaction(id:String):Boolean

		logInUser(
			email: String!
			password: String!
		): UserType
	}

`;

const resolvers = {
	Query: {

		getUsers: () => {
			return User.find({});
		},

		getVenues: () => {
			return Venue.find({});
		},

		getCateringServices: () => {
			return CateringService.find({});
		},

		getTransactions: () => {
			return Transaction.find({});
		},

		getUser: (_, args) => {
			return User.findById(args.id);
		},

		getVenue: (parent, args) => {
			return Venue.findById(args.id);
		},

		getCateringService: (_, args) => {
			return CateringService.findById(args.id);
		},

		getTransaction: (parent, args) => {
			return Transaction.findById(args.id);
		}
	},

	Mutation: {
		createUser: (_, args) => {
			let newUser = User({
				firstName: args.firstName,
				lastName: args.lastName,
				contactNumber: args.contactNumber,
				email: args.email,
				password: bcrypt.hashSync(args.password),
				role: "user"
			});

			return newUser.save();
		},

		createVenue: (_, args) => {
			let newVenue = Venue({
				name: args.name,
				floorArea: args.floorArea,
				maxGuest: args.maxGuest,
				description: args.description,
				price: args.price
			});

			return newVenue.save();
		},

		createCateringService: (_, args) => {
			let newCateringService = CateringService({
				packageName: args.packageName,
				pricePerHead: args.pricePerHead,
				description: args.description,
				menu: args.menu
			
			});

			return newCateringService.save();
		},

		createTransaction: (_, args) => {

			let newTransaction = Transaction({
				venueId: args.venueId,
				userId: args.userId,
				eventType: args.eventType,
				guestNumber: args.guestNumber,
				cateringService: args.cateringService,
				date:args.date,
				time:args.time
			});

			return newTransaction.save();
		},



		updateUser: (_, args) => {
			let condition = args.id
				
			console.log(args);
			let updates = { 
				firstName: args.firstName,
				lastName: args.lastName,
				contactNumber: args.contactNumber,
				email: args.email,
				password: args.password,
				role: args.role

			};
			return User.findByIdAndUpdate(condition, updates);


		},

		updateVenue: (_, args) => {
			let condition = args.id

			let updates = { 
				name: args.name,
				floorArea: args.floorArea,
				maxGuest: args.maxGuest,
				description: args.description,
				price: args.price

			};
			return Venue.findByIdAndUpdate(condition, updates);


		},

		updateCateringService: (_, args) => {
			let condition = args.id
			
			let updates = { 
				packageName: args.packageName,
				pricePerHead: args.pricePerHead,
				description: args.description,
				menu: args.menu

			};
			return CateringService.findByIdAndUpdate(condition, updates);
		},


		updateTransaction: (_, args) => {
			let condition = args.id

			let updates = { 
				venueId: args.venueId,
				userId: args.userId,
				eventType: args.eventType,
				guestNumber: args.guestNumber,
				cateringService: args.cateringService,
				date: args.date,
				time: args.time

			};
			return Transaction.findByIdAndUpdate(condition, updates);
		},

		deleteUser: (_, args) => {
			let condition = args.id;

			return User.findByIdAndDelete(condition).then((user, err) => {
				if (err || !user) {
					console.log("delete failed. no user found");
					return false;
				}
				console.log("user deleted");
				return true;
			});
		},

		deleteVenue: (_, args) => {
			let condition = args.id;

			return Venue.findByIdAndDelete(condition).then((venue, err) => {
				if (err || !venue) {
					console.log("delete failed. no venue found");
					return false;
				}
				console.log("venue deleted");
				return true;
			});
		},


		deleteCateringService: (_, args) => {
			let condition = args.id;

			return CateringService.findByIdAndDelete(condition).then((cateringService, err) => {
				if (err || !cateringService) {
					console.log("delete failed. no catering service found");
					return false;
				}
				console.log("catering service deleted");
				return true;
			});
		},

		deleteTransaction: (_, args) => {
			let condition = args.id;

			return Transaction.findByIdAndDelete(condition).then((transaction, err) => {
				if (err || !transaction) {
					console.log("delete failed. no transaction found");
					return false;
				}
				console.log("transaction deleted");
				return true;
			});
		},

		logInUser: (_, args) => {
			console.log("trying to log in...");
			console.log(args);

			// returns the member in our members collection
			// with the same value as args.firstName
			return User.findOne({ email: args.email }).then(user => {
				console.log(user);

				if (user === null) {
					console.log("user not found");
					return null;
				}

				console.log(user.password);
				console.log(args.password);

				let hashedPassword = bcrypt.compareSync(
					args.password,
					user.password
				);

				// console.log(hashedPassword);

				// if hashedPasword is false, output in the console wrong password
				if (!hashedPassword) {
					console.log("wrong password");
					return null;
				}
			
				else {
					// successful login
					console.log(user);
					return user;
				}
			});
		}

		// TransactionType:{
		// 	venue: (parent, args)=>{
		// 		return Venue.findById(parent.venueId)
		// 	}
		// }



	}	
}

const server = new ApolloServer({
	typeDefs,
	resolvers
});

module.exports = server;


