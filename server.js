const express = require ("express")
const mongoose = require("mongoose")


const app = express();

const databaseUrl = process.env.DATABASE_URL ||
	"mongodb+srv://lizchuang:lizchuang@nosqlsession-yfu2l.mongodb.net/kairos_event_place_db?retryWrites=true&w=majority"

mongoose.connect(
	databaseUrl,
	{
		useNewUrlParser : true,
		useFindAndModify: false,
		useUnifiedTopology: true

	}
)

mongoose.connection.once("open", () => {
	console.log("Now connected to the online MongoDB server")
})

const server = require("./queries/queries.js");


server.applyMiddleware({
	app,
	// path: "/batch43"
})

let port = process.env.PORT ||4000



app.listen(port, () => {
  console.log(`🚀  Server ready at http://localhost:${port}${server.graphqlPath}`);
})


