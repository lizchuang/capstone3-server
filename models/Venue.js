const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const venueSchema = new Schema({
	name: {
		type: String,
		required: true
	},

	floorArea: {
		type: Number,
		required: true
	},

	maxGuest: {
		type: Number,
		required: true,

	},

	description: {
		type: String,
		required: true
	},

	price: {
		type: Number,
		required: true
	},

})

module.exports = mongoose.model("Venue", venueSchema);
