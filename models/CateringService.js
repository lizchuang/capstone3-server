const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const cateringServiceSchema = new Schema({
	packageName: {
		type: String,
		required: true
	},

	pricePerHead: {
		type: Number,
		required: true
	},

	description: {
		type: String,
		required: true,
	},
	menu: {
		type: String,
		required: true,
	}


})

module.exports = mongoose.model("CateringService", cateringServiceSchema);