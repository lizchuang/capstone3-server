const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const transactionSchema = new Schema({
	venueId: {
		type: String,
		required: true
	},

	venue:{
		type: String
	},

	userId: {
		type: String,
	},

	eventType: {
		type: String,
		required: true,

	},

	guestNumber: {
		type: Number,
		required: true,

	},

	cateringService: {
		type: String,
		required: true
	},

	date: {
		type: mongoose.Schema.Types.Date,
		required: true
	},

	time: {
		type: String,
		required: true
	}

})

module.exports = mongoose.model("Transaction", transactionSchema);